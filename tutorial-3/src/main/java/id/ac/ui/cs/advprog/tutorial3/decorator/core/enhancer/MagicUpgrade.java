package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;
    int upgraded;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;

    }

    @Override
    public String getName() {
        return "Magic " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int minimum = 15;
        int maximum = 20;
        int range = maximum-minimum+1;
        Random random = new Random();
        upgraded = random.nextInt(range)+minimum;

        if (weapon != null){
            return weapon.getWeaponValue() + upgraded;
        }
        else{
            return upgraded;
        }
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Magic " + weapon.getDescription();
    }
}
