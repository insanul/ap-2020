package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    int upgraded;

    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;

    }

    @Override
    public String getName() {
        return "Chaos " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int minimum = 50;
        int maximum = 55;
        int range = maximum-minimum+1;
        Random random = new Random();
        upgraded = random.nextInt(range)+minimum;

        if (weapon != null){
            return weapon.getWeaponValue() + upgraded;
        }
        else{
            return upgraded;
        }
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Chaos " + weapon.getDescription();
    }
}
