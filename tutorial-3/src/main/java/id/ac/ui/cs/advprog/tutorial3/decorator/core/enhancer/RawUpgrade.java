package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    int upgraded;

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;

    }

    @Override
    public String getName() {
        return "Raw " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int minimum = 5;
        int maximum = 10;
        int range = maximum-minimum+1;
        Random random = new Random();
        upgraded = random.nextInt(range)+minimum;
        if (weapon != null){
            return weapon.getWeaponValue() + upgraded;
        }
        else{
            return upgraded;
        }
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Raw " + weapon.getDescription();
    }
}
