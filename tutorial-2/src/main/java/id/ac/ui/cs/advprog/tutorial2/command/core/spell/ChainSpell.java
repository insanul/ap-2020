package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    protected ArrayList<Spell> listOfSpell;

    public ChainSpell(ArrayList<Spell> listOfSpell) {
        this.listOfSpell = listOfSpell;
    }

    @Override
    public void cast() {
        for (Spell spell : listOfSpell) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        int sizeList = listOfSpell.size();
        for (int i = sizeList-1 ; i >= 0 ; i--){
            listOfSpell.get(i).cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
