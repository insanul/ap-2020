package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals("Magic Longbow",magicUpgrade.getName());
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //TODO: Complete me
        assertEquals("Magic Big Longbow",magicUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int upgradeValue = magicUpgrade.getWeaponValue();
        assertTrue(upgradeValue >= 30 && upgradeValue <= 35);
    }
}
