package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
        //TODO: Complete me
    public Sword(){
        weaponName = "Sword";
        weaponValue = 25;
        weaponDescription = "Great Sword";
    }
}
