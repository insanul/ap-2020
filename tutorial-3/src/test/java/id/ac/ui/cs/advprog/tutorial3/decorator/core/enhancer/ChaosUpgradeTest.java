package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;


import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Gun;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChaosUpgradeTest {

    private ChaosUpgrade chaosUpgrade;
    @BeforeEach
    public void setUp(){
        chaosUpgrade = new ChaosUpgrade(new Gun());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals("Chaos Gun",chaosUpgrade.getName());
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //TODO: Complete me
        assertEquals("Chaos Automatic Gun",chaosUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int upgradeValue = chaosUpgrade.getWeaponValue();
        assertTrue(upgradeValue >= 70 && upgradeValue <= 75);
    }
}
