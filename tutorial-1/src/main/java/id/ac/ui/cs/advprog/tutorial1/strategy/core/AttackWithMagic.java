package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {

    @Override
    public String attack() {
        return "BIMSALAMBIM!";
    }

    @Override
    public String getType() {
        return "Sihir";
    }
}
