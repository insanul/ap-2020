package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild=guild;
        }

        @Override
        public void update() {
                if((this.guild.getQuest().getType().equals("D")) || (this.guild.getQuest().getType().equals("E"))){
                        this.getQuests().add(this.guild.getQuest());
                }
        }
}
